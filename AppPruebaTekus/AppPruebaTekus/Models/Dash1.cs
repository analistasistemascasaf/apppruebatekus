﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppPruebaTekus.Models
{
    public class Dash1
    {
        //name: 'Chrome',
        //    y: 61.41,
        //    sliced: true,
        //    selected: true
        public string name { get; set; }
        public decimal y { get; set; }
        public bool sliced { get; set; }
        public bool selected { get; set; }
    }

    public class Dash2
    {
        public string name { get; set; }
        public List<object> data { get; set; }
    }
}