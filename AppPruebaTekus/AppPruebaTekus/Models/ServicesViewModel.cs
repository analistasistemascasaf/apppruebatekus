﻿using ModelosPruebasTekus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppPruebaTekus.Models
{
    public class ServicesViewModel
    {
        public Service Service { get; set; }

        public List<ServiceCountry> ServiceCountries { get; set; }
    }
}