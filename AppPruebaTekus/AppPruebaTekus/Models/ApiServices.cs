﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace AppPruebaTekus.Models
{
    public class ApiServices
    {
        public async Task<Response> GetList<T>(string urlBase, string prefix, string controller)
        {
            try
            {
                var client = new HttpClient();
                client.BaseAddress = new Uri(urlBase);
                var url = $"{prefix}{controller}";
                var response = await client.GetAsync(url);
                var answer = await response.Content.ReadAsStringAsync();
                var answer2 = new StringContent(answer, Encoding.UTF8, "application/json");
                if (!response.IsSuccessStatusCode)
                {
                    return new Response
                    {
                        IsSucces = false,
                        Message = answer2.ReadAsStringAsync().Result.ToString()
                    };
                }
                var list = JsonConvert.DeserializeObject<T>(answer);

                return new Response
                {
                    IsSucces = true,
                    Result = list
                };
            }
            catch (Exception ex)
            {
                return new Response
                {
                    IsSucces = false,
                    Message = ex.Message
                };
            }
        }

        public class Response
        {
            public bool IsSucces { get; set; }

            public string Message { get; set; }

            public object Result { get; set; }
        }
    }
}