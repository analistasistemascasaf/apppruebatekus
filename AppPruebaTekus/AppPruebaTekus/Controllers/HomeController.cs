﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AppPruebaTekus.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Prueba tecnica Tekus";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "hernang18@gmail.com";

            return View();
        }
    }
}