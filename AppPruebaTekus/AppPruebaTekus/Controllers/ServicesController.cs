﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AppPruebaTekus.Models;
using ModelosPruebasTekus;

namespace AppPruebaTekus.Controllers
{
    public class ServicesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Services
        public async Task<ActionResult> Index()
        {
            var services = db.Services.Include(s => s.Supplier);
            return View(await services.ToListAsync());
        }

        // GET: Services/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Service service = await db.Services.FindAsync(id);
            if (service == null)
            {
                return HttpNotFound();
            }
            return View(service);
        }

        // GET: Services/Create
        public ActionResult Create()
        {
            ViewBag.SupplierId = new SelectList(db.Suppliers, "Id", "Name");
            return View();
        }

        // POST: Services/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(Service service)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Services.Add(service);
                    await db.SaveChangesAsync();
                    return RedirectToAction("Edit", new { id = service.Id });
                }

                ViewBag.SupplierId = new SelectList(db.Suppliers, "Id", "Name", service.SupplierId);
                return View(service);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(String.Empty, ex.InnerException.InnerException.Message);

                ViewBag.SupplierId = new SelectList(db.Suppliers, "Id", "Name", service.SupplierId);
                return View(service);
            }
        }

        // GET: Services/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Service service = db.Services.Find(id);
            if (service == null)
            {
                return HttpNotFound();
            }
            var serviceCountries = db.ServiceCountries.Where(w => w.ServiceId == id).ToList();
            var servicesViewModel = new ServicesViewModel
            {
                Service = service,
                ServiceCountries = serviceCountries
            };
            ViewBag.SupplierId = new SelectList(db.Suppliers, "Id", "Name", service.SupplierId);
            ApiServices apiServices = new ApiServices();
            var response = await apiServices.GetList<List<Root>>("https://restcountries.com/", "v2/all", "");
            List<Root> roots = new List<Root>();
            roots = (List<Root>)response.Result;
            ViewBag.CountryId = new SelectList(roots, "numericCode", "name");
            return View(servicesViewModel);
        }

        // POST: Services/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit( Service service)
        {
            if (ModelState.IsValid)
            {
                db.Entry(service).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.SupplierId = new SelectList(db.Suppliers, "Id", "Name", service.SupplierId);
            return View(service);
        }

        // GET: Services/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Service service = await db.Services.FindAsync(id);
            if (service == null)
            {
                return HttpNotFound();
            }
            return View(service);
        }

        // POST: Services/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Service service = await db.Services.FindAsync(id);
            db.Services.Remove(service);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        public async Task<ActionResult> DeleteCountry(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ServiceCountry serviceCountry = await db.ServiceCountries.FindAsync(id);
            int serviceid = serviceCountry.ServiceId;
            db.ServiceCountries.Remove(serviceCountry);
            await db.SaveChangesAsync();
            return RedirectToAction("Edit", new { id = serviceid });
        }

        public  JsonResult Saved(int id,string Name,string Email,double Price,int SupplierId)
        {
            try
            {
                var service = db.Services.Find(id);
                service.Name = Name;
                service.Email = Email;
                service.SupplierId = SupplierId;
                service.Price=Price;
                db.Entry(service).State = EntityState.Modified;
                db.SaveChanges();
                return Json(new ResponseMessage { Status = true,Message="Se guarda correctamente" });
            }
            catch (Exception ex)
            {

                return Json(new ResponseMessage { Status = false, Message =ex.Message });
            }
        }

        public JsonResult AddCountry(int id, string CountryId,string CountryName)
        {
            try
            {
                if(CountryId==null || CountryId==string.Empty)
                {
                    return Json(new ResponseMessage { Status = false, Message = "Debe Seleccionar un Pais" });
                }
                var serviceCountry = db.ServiceCountries.Where(w => w.ServiceId == id && w.Code == CountryId).FirstOrDefault();
                if(serviceCountry!=null)
                {
                    return Json(new ResponseMessage { Status = false, Message = "Este pais ya esta registrado con este servicio" });
                }
                serviceCountry = new ServiceCountry
                {
                    ServiceId = id,
                    Code = CountryId,
                    Name = CountryName,
                };
                db.ServiceCountries.Add(serviceCountry);
                db.SaveChanges();               
                return Json(new ResponseMessage { Status = true, Message = "Se guarda correctamente" });
            }
            catch (Exception ex)
            {

                return Json(new ResponseMessage { Status = false, Message = ex.Message });
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
