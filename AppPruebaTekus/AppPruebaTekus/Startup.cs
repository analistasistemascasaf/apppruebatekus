﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AppPruebaTekus.Startup))]
namespace AppPruebaTekus
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
