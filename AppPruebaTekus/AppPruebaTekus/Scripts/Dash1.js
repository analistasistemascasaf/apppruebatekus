﻿$(document).ready(function () {
    $.ajax({
        type: "GET",
        contentType: "application/json; charseft=utf-8",
        dataType: "json",
        url: urldash1,
        error: function () {
            alert("Error en los datos")
        },
        success: function (data) {
            console.log(data);
            Dash1(data);
        }

    })
    $.ajax({
        type: "GET",
        contentType: "application/json; charseft=utf-8",
        dataType: "json",
        url: urldash2,
        error: function () {
            alert("Error en los datos")
        },
        success: function (data2) {
            console.log(data2);
            Dash2(data2);
        }

    })
});

function Dash1(data) {
    Highcharts.chart('container', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Paises/Servicios prestados'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        accessibility: {
            point: {
                valueSuffix: '%'
            }
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        },
        series: [{
            name: 'Pais',
            colorByPoint: true,
            data:data
        }]
    });
}

function Dash2(data2) {
    Highcharts.chart('container2', {

        title: {
            text: 'Servicios'
        },

        subtitle: {
            text: 'Cantidad de paises que se presta'
        },

        yAxis: {
            title: {
                text: 'Numero de Paises'
            }
        },

        xAxis: {
            accessibility: {
                rangeDescription: ''
            }
        },

        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle'
        },

        plotOptions: {
            series: {
                label: {
                    connectorAllowed: false
                },
                pointStart: 0
            }
        },

        series: data2,

        responsive: {
            rules: [{
                condition: {
                    maxWidth: 500
                },
                chartOptions: {
                    legend: {
                        layout: 'horizontal',
                        align: 'center',
                        verticalAlign: 'bottom'
                    }
                }
            }]
        }

    });

}