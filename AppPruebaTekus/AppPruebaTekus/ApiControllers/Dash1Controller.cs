﻿using AppPruebaTekus.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace AppPruebaTekus.ApiControllers
{
    public class Dash1Controller : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        /// <summary>
        /// Paises y su porcentaje de servicios
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("api/GetDash1")]
        [ResponseType(typeof(List<Dash1>))]
        public IHttpActionResult GetDash1()
        {
            var GroupCountry = (db.ServiceCountries.GroupBy(g => g.Name)
                    .Select(s => new Dash1
                    {
                        name = s.Key,
                        y = s.Count()
                    })).ToList();
            return Ok(GroupCountry);

        }

        [HttpGet]
        [Route("api/GetDash2")]
        [ResponseType(typeof(List<Dash2>))]
        public IHttpActionResult GetDash2()
        {
            var Groupservice = (db.ServiceCountries.GroupBy(g => g.ServiceId)
                    .Select(s => new
                    {
                        Id = s.Key
                    })).ToList();
            List<Dash2> dash2s = new List<Dash2>();
            foreach (var item in Groupservice)
            {
                var service = db.Services.Find(item.Id);
                var x = service.ServicesCountries.ToArray();
                List<object> dat = new List<object>();
                foreach (var row in service.ServicesCountries)
                {
                    dat.Add(service.ServicesCountries.Count);
                }

                dash2s.Add(new Dash2 { name = service.Name, data = dat });
            }
            return Ok(dash2s);

        }
    }
}
