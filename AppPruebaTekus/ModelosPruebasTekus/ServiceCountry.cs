﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelosPruebasTekus
{
    public class ServiceCountry
    {
        [Key]
        public int Id { get; set; }

        [Index("IX_ServiceCountry_Master", IsUnique = true, IsClustered = false, Order = 1)]
        [Required(ErrorMessage = "Codigo Obligatorio")]
        [StringLength(20, MinimumLength = 3)]
        [Display(Name = "Codigo")]
        public string Code { get; set; }

        [Required(ErrorMessage = "Nombre Obligatorio")]
        [Display(Name = "Nombre")]
        public string Name { get; set; }

        [Index("IX_ServiceCountry_Master", IsUnique = true, IsClustered = false, Order = 2)]
        public int ServiceId { get; set; }

        public virtual Service Service { get; set; }

    }
}
