﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelosPruebasTekus
{
    public class Service
    {
        [Key]
        public int Id { get; set; }

        [Index(IsUnique = true)]
        [Required(ErrorMessage = "Nombre Obligatorio")]
        [StringLength(100, MinimumLength = 3)]
        [Display(Name = "Nombre Servicio")]
        public string Name { get; set; }

        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        [Required(ErrorMessage = "Correo Obligatorio")]
        [Display(Name = "Correo")]
        public string Email { get; set; }

        public int SupplierId { get; set; }

        [Range(0,double.PositiveInfinity, ErrorMessage = "Valor/Hora debe ser positivo")]
        [Required(ErrorMessage = "Valor/Hora Obligatorio")]
        [Display(Name = "Valor/Hora")]
        public double Price { get; set; }

        public virtual Supplier Supplier { get; set; }

        public virtual ICollection<ServiceCountry> ServicesCountries { get; set; }

        [JsonIgnore]
        [NotMapped]
        public int CountryId { get; set; }
    }
}
