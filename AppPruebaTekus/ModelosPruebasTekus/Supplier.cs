﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelosPruebasTekus
{
    public class Supplier
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Nit Obligatorio")]
        [Index(IsUnique = true)]
        [StringLength(20, MinimumLength = 3)]
        [Display(Name ="Nit")]
        public string Nit { get; set; }

        [Required(ErrorMessage = "Nombre Obligatorio")]
        [Display(Name = "Nombre Proveedor")]
        public string Name { get; set; }

        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        [Required(ErrorMessage = "Correo Obligatorio")]
        [Display(Name = "Correo")]
        public string Email { get; set; }

        public virtual ICollection<Service> Services { get; set; }
    }
}
